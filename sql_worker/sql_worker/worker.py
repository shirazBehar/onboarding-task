import json

from liblkpo.rpc.server import RPCService, exposed
from liblkpo.manager.worker_base import WorkerBase
from sql_worker.db_handler import DBHandler
from sql_worker.assets_tables import Asset


DB_PATH = 'postgresql+psycopg2://postgres:dt2016@localhost/assets'
db_handler = DBHandler(db_path=DB_PATH)


class SqlService(RPCService):
    def __init__(self, worker):
        super().__init__()
        self._worker = worker

    @exposed
    def get_asset(self, asset_id: int):
        asset = db_handler.get_session().query(Asset).filter_by(id=asset_id).first()
        return {
                'id': asset.id, 'name': asset.name
                }

    @exposed
    def get_all_assets(self):
        return json.dumps([{
                'id': asset.id, 'name': asset.name
                } for asset in db_handler.get_session().query(Asset)
                ])

    @exposed
    def create_asset(self, data):
        session = db_handler.get_session()
        if not 'name' in data:
            return json.dumps({
                'error': 'Bad Request',
                'message': 'Name not provided'
            }), 400
        if len(data['name']) < 4:
            return json.dumps({
                'error': 'Bad Request',
                'message': 'Name must be contain minimum of 4 letters'
            }), 400
        asset = Asset(name=data['name'])
        session.add(asset)
        session.commit()
        return json.dumps({
                'id': asset.id, 'name': asset.name
                })

    @exposed
    def update_asset(self, id: int, data):
        session = db_handler.get_session()
        if not 'name' in data:
            return json.dumps({
                'error': 'Bad Request',
                'message': 'Name not provided'
            }), 400
        if len(data['name']) < 4:
            return json.dumps({
                'error': 'Bad Request',
                'message': 'Name must be contain minimum of 4 letters'
            }), 400
        asset = session.query(Asset).filter_by(id=id).first()
        asset.name = data['name']
        session.merge(asset)
        session.commit()
        return json.dumps({
                    'id': asset.id, 'name': asset.name
                    })

    @exposed
    def delete_asset(self, id: int):
        session = db_handler.get_session()
        asset = session.query(Asset).filter_by(id=id).first()
        session.delete(asset)
        session.commit()
        return json.dumps({
            'success': 'Data deleted sucessfully'
        })

class SqlWorker(WorkerBase):
    @property
    def sql_service(self):
        return self._server.service('sql')

    def config(self):
        self._server.add_service("sql", SqlService(self))

def main():
    worker = SqlWorker()
    worker.main()
