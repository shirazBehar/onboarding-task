from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, session

from sql_worker.assets_tables import Base


class DBHandler:
    def __init__(self, db_path: str):
        self.db_path = db_path
        self._init_db()

    def _init_db(self):
        self.engine = create_engine(self.db_path)
        self.session_maker = sessionmaker(bind=self.engine)
        Base.metadata.create_all(self.engine)

    def get_session(self):
        return self.session_maker()
