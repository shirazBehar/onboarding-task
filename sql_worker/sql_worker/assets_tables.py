from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.exc import IntegrityError
from sqlalchemy.sql import func
from sqlalchemy.orm.session import Session
from . import db_handler

Base = declarative_base()

class Asset(Base):
    __tablename__ = 'assets_table'

    # Serial
    id = Column(Integer, primary_key=True)
    name = Column(String(100), index=True, unique=True)
