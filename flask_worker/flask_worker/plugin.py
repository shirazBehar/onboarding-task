from requests import get, post, delete, put
from urllib3.util import Url
from typing import Union

from liblkpo.manager.plugins import ManagerPlugin
from liblkpo.rpc.server import RPCService, exposed

class AssetsService(RPCService):
    def __init__(self, addr: str, port: int):
        super().__init__()
        self._addr = addr
        self._port = port

    def _request(self, method: Union[get, post, delete, put], endpoint: str, data={}):
        req_url = Url(scheme='https', host=self._addr,
                   port=self._port, path=f"api/{endpoint}").url
        return method(req_url, verify=False, json=data).json()

    @exposed
    def get_all_assets(self):
        return self._request(method=get, endpoint="get_all_assets")

    @exposed
    def get_asset(self, asset_id: int):
        return self._request(method=get, endpoint=f"get_asset/{asset_id}")

    @exposed
    def create_asset(self, asset: dict):
        return self._request(method=post, endpoint="create_asset", data=asset)

    @exposed
    def update_asset(self, asset_id: int, asset: dict):
        return self._request(method=put, endpoint=f"update_asset/{asset_id}", data=asset)

    @exposed
    def delete_asset(self, asset_id: int):
        return self._request(method=delete, endpoint=f"delete_asset/{asset_id}")


class AssetsPlugin(ManagerPlugin):
    def start(self):
        self.manager.add_service("assets_api", AssetsService(self._api_addr, self._api_port))

    @property
    def _api_addr(self):
        return self.manager._service.get_config("workers.flask_worker.listen_addr")

    @property
    def _api_port(self):
        return self.manager._service.get_config("workers.flask_worker.listen_port")

    def stop(self):
        self.manager.remove_service("assets_api")
