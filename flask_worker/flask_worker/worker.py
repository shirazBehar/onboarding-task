from flask import Flask, request
from flask_cors import CORS
from flask_cors import cross_origin

app = Flask(__name__, instance_relative_config=True)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

@app.route("/api/get_all_assets")
@cross_origin()
def get_all_assets():
    return app.sql_service.get_all_assets()


@app.route("/api/get_asset/<int:asset_id>")
@cross_origin()
def get_asset(asset_id):
    return app.sql_service.get_asset(asset_id)


@app.route("/api/create_asset/<string:name>", methods=['POST'])
@cross_origin()
def create_asset(name):
    return app.sql_service.create_asset({ 'name' : name})


@app.route("/api/update_asset/<int:asset_id>", methods=['PUT'])
@cross_origin()
def update_asset(asset_id):
    return app.sql_service.update_asset(asset_id, request.get_json())


@app.route("/api/delete_asset/<int:asset_id>", methods=['DELETE'])
@cross_origin()
def delete_asset(asset_id):
    return app.sql_service.delete_asset(asset_id)
