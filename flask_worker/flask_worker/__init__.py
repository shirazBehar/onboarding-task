from flask_worker.worker import app

def initialize_worker(worker, app):
    app.sql_service = worker.manager_api.worker('sql_worker').sql

if __name__ == "__main__":
	worker = initialize_worker()
	worker.main()
